import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTheme {
  static const lightPrimary = Color(0xFF0F549A);
  static const lightBackground = Color(0xFF052647);
  static const lightOnBackground = Color(0xFFF37878);
  static const darkPrimary = Color(0xFF151D3B);
  static const darkBackground = Color(0xFF3A3845);
  static const darkOnBackground = Color(0xFFF3F3F3);
  static const ColorScheme lightColorScheme = ColorScheme(    
    background: Colors.transparent,
    surface: Color(0xFFFF7F3F),
    primary: lightPrimary,
    secondary: Color.fromARGB(255, 31, 31, 31),
    onBackground: lightOnBackground,
    onSurface: Color(0xFF616161),
    onPrimary: Colors.white,
    onSecondary: Color(0xFF242424),
    error: Colors.white,
    onError: Colors.white,
    brightness: Brightness.light,
  );

  static const ColorScheme darkColorScheme = ColorScheme(
    background: darkBackground,
    surface: Color(0xFF242424),
    primary: darkPrimary,
    secondary: Color(0xFFF5F5F5),
    onBackground: darkOnBackground,
    onSurface: Colors.white,
    onPrimary: Colors.white,
    onSecondary: Color(0xFF242424),
    error: Colors.white,
    onError: Colors.white,
    brightness: Brightness.dark,
  );

  static ThemeData light() {
    return ThemeData(
      colorScheme: lightColorScheme,
      appBarTheme: AppBarTheme(
        color: lightColorScheme.background,
        elevation: 0,
        iconTheme: IconThemeData(color: lightColorScheme.primary),
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      iconTheme: IconThemeData(color: lightColorScheme.onPrimary),
      canvasColor: lightColorScheme.background,
      scaffoldBackgroundColor: lightColorScheme.background,
      highlightColor: Colors.transparent,
      snackBarTheme: SnackBarThemeData(
        behavior: SnackBarBehavior.floating,
        backgroundColor: Color.alphaBlend(
          Colors.white.withOpacity(0.80),
          Colors.white,
        ),
      ),
      textTheme: const TextTheme(titleMedium: TextStyle())
    );
  }

  static ThemeData dark() {
    return ThemeData(
      colorScheme: darkColorScheme,
      appBarTheme: AppBarTheme(
        color: darkColorScheme.background,
        elevation: 0,
        iconTheme: IconThemeData(color: darkColorScheme.primary),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      iconTheme: IconThemeData(color: darkColorScheme.onPrimary),
      canvasColor: darkColorScheme.background,
      scaffoldBackgroundColor: darkColorScheme.background,
      highlightColor: Colors.transparent,
      snackBarTheme: SnackBarThemeData(
        behavior: SnackBarBehavior.floating,
        backgroundColor: Color.alphaBlend(
          Colors.white.withOpacity(0.80),
          Colors.white,
        ),
      ),
    );
  }
}
