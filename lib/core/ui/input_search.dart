import 'package:flutter/material.dart';

typedef OnSearchTap = Function(String);
typedef OnChange = Function(String);

class InputSearchField extends StatefulWidget {
  final TextEditingController controller;
  final String? hintText;
  final EdgeInsets? contentPadding;
  final Radius borderRadius;
  final Color? backgroundColor;
  final Color? textColor;
  final Size iconSize;
  final Color iconColor;
  final Color? iconBackgroundColor;
  final bool? showIndicator;
  final OnSearchTap onSearchTap;
  final OnChange onChange;
  const InputSearchField({
    required this.controller,
    this.hintText,
    this.contentPadding = const EdgeInsets.all(8.0),
    this.borderRadius = const Radius.circular(16.0),
    this.backgroundColor,
    this.textColor,
    this.iconSize = const Size(32, 32),
    this.iconColor = Colors.black,
    this.iconBackgroundColor,
    this.showIndicator,
    required this.onSearchTap,
    required this.onChange,
    Key? key,
  }):super(key: key);

  @override
  _InputSearchFieldState createState() => _InputSearchFieldState();
}

class _InputSearchFieldState extends State<InputSearchField> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(left: 12, right: 12),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: widget.borderRadius,
                    bottomLeft: widget.borderRadius),
              ),
              child: TextField(
                controller: widget.controller,
                decoration: InputDecoration(
                  isDense: true,
                  fillColor: Colors.white,
                  filled: true,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  border: InputBorder.none,
                  contentPadding: widget.contentPadding ?? const EdgeInsets.symmetric(vertical: 5.0, horizontal: 12.0),
                  hintText: widget.hintText ?? "",
                  hintStyle: TextStyle(
                      height: 1,
                      fontSize: 14,
                      color: widget.textColor ?? Theme.of(context).colorScheme.primary),
                ),
                style: TextStyle(
                    color: widget.textColor ?? Theme.of(context).colorScheme.primary,),
                onChanged: (text) {
                  widget.onChange(text);
                },
              ),
            ),
          ),
          widget.showIndicator != null && widget.showIndicator == true
              ? const SizedBox(
                  width: 1,
                )
              : const SizedBox(
                  width: 0,
                ),
          InkWell(
            onTap: () {
              widget.onSearchTap(widget.controller.text);
            },
            child: Container(
              height: widget.iconSize.height,
              width: widget.iconSize.width,
              padding: const EdgeInsets.only(top: 6, bottom: 6),
              decoration: BoxDecoration(
                color: widget.iconBackgroundColor ?? const Color(0xFFe5c122),
                borderRadius: BorderRadius.only(
                    topRight: widget.borderRadius,
                    bottomRight: widget.borderRadius),
              ),
              child: Icon(Icons.search, color: widget.iconColor,),
            ),
          ),
        ]);
  }
}
