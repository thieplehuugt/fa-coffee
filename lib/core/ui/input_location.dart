import 'package:flutter/material.dart';

import '../../models/location.dart';
import 'chips_input/chips_input.dart';

typedef OnChanged = Function(Location?);

class LocationInputField extends StatefulWidget {
  final InputDecoration? decoration;
  final String? hintText;
  final EdgeInsets? contentPadding;
  final double? height;
  final Color? color;
  final Color? backgroundColor;
  final BorderRadius? borderRadius;
  final Location? dropdownValue;
  final List<Location> items;
  final OnChanged onChanged;
  const LocationInputField({
    this.dropdownValue,
    required this.items,
    this.height,
    this.color,
    this.backgroundColor,
    this.borderRadius,
    this.decoration,
    this.hintText,
    this.contentPadding,
    required this.onChanged,
    Key? key,
  }):super(key: key);
  @override
  _LocationInputFieldState createState() => _LocationInputFieldState();
}

class _LocationInputFieldState extends State<LocationInputField> {
  Location dropdownValue = const Location(id: 0, name: "指定なし");
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height ?? 32,
      padding: const EdgeInsets.only(left: 5, right: 5),
      decoration: BoxDecoration(
        color: widget.backgroundColor ?? Colors.white,
        borderRadius: widget.borderRadius,
      ),
      child: Row(
        children: [
          Icon(
            Icons.add_location,
            color: widget.color ?? Theme.of(context).colorScheme.primary,
          ),
          DropdownButtonHideUnderline(
            child: DropdownButton<Location>(
              iconSize: 0.0,
              value: dropdownValue,
              style: const TextStyle(color: Colors.deepPurple),
              onChanged: (Location? newValue) {
                widget.onChanged(newValue);
                setState(() {
                  if(newValue != null){
                    dropdownValue = newValue;
                  }                  
                });
              },
              items: widget.items
                  .map<DropdownMenuItem<Location>>((Location value) {
                return DropdownMenuItem<Location>(
                  value: value,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 5,
                    ),
                    child: Text(
                      value.name,
                      style: TextStyle(
                          color: widget.color ?? Theme.of(context).colorScheme.primary),
                    ),
                  ),
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}


typedef OnItemSelected = Function(List<Location>);

class LocationMultiSelectInputField extends StatefulWidget {
  final InputDecoration? decoration;
  final String? hintText;
  final EdgeInsets? contentPadding;
  final double? height;
  final Color? color;
  final Color? backgroundColor;
  final BorderRadius? borderRadius;
  final Location? dropdownValue;
  final List<Location> items;
  final OnItemSelected onItemSelected;
  const LocationMultiSelectInputField({
    this.dropdownValue,
    required this.items,
    this.height,
    this.color,
    this.backgroundColor,
    this.borderRadius,
    this.decoration,
    this.hintText,
    this.contentPadding,
    required this.onItemSelected,
    Key? key,
  }):super(key: key);
  @override
  _LocationMultiSelectInputFieldState createState() =>
      _LocationMultiSelectInputFieldState();
}

class _LocationMultiSelectInputFieldState
    extends State<LocationMultiSelectInputField> {
  Location dropdownValue = const Location(id: 0, name: "指定なし");
  final GlobalKey<ChipsInputState> _chipKey = GlobalKey();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: widget.backgroundColor ?? Colors.white,
        borderRadius: widget.borderRadius,
      ),
      child: Row(
        children: [
          Flexible(
              child: ChipsInput(
            key: _chipKey,
            keyboardAppearance: Brightness.dark,
            textCapitalization: TextCapitalization.words,
            decoration: InputDecoration(
                isDense: true,
                border: InputBorder.none,
                fillColor: Colors.white,
                contentPadding:
                    const EdgeInsets.only(top: 4.0, bottom: 4.0, left: 12, right: 8),
                prefixIconConstraints: const BoxConstraints(
                  minHeight: 24,
                ),
                prefixIcon: Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Icon(
                    Icons.add_location,
                    size: 24,
                    color: widget.color ?? Theme.of(context).colorScheme.primary,
                  ),
                ),
                hintText: "都道府県",
                hintStyle: TextStyle(
                    fontSize: 14,
                    height: 1,
                    color: Theme.of(context).colorScheme.primary)),
            textStyle: TextStyle(height: 1, fontSize: 14, color: Theme.of(context).colorScheme.onSecondary),
            findSuggestions: (String query) {
              // print("Query: '$query'");
              if (query.isNotEmpty) {
                var lowercaseQuery = query.toLowerCase();
                return widget.items.where((location) {
                  return location.name
                      .toLowerCase()
                      .contains(query.toLowerCase());
                }).toList(growable: false)
                  ..sort((a, b) => a.name
                      .toLowerCase()
                      .indexOf(lowercaseQuery)
                      .compareTo(b.name.toLowerCase().indexOf(lowercaseQuery)));
              }
              // return <AppProfile>[];
              return widget.items;
            },
            onChanged: (data) {
              widget.onItemSelected(List<Location>.from(data));
            },
            chipBuilder: (context, state, location) {
              return Container(
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.green,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text((location as Location).name,
                        style: const TextStyle(
                            height: 1, fontSize: 10, color: Colors.white)),
                    InkWell(
                      onTap: () {
                        state.deleteChip(location);
                      },
                      child: const Icon(
                        Icons.remove_circle,
                        size: 12,
                      ),
                    )
                  ],
                ),
              );

              /*InputChip(
                    backgroundColor: Colors.green,
                    padding: EdgeInsets.symmetric(horizontal: 3, vertical: 0),
                    key: ObjectKey(location),
                    label: Text(location.name,
                        style: TextStyle(height: 1, fontSize: 10)),
                    onDeleted: () => state.deleteChip(location),
                    deleteIcon: Icon(
                      Icons.remove_circle,
                      size: 16,
                    ),
                  ),
                );*/
            },
            suggestionBuilder: (context, state, profile) {
              return profile != null ? ListTile(
                key: ObjectKey(profile),
                title: Text(
                  (profile as Location).name,
                  style: const TextStyle(height: 1, fontSize: 14),
                ),
                onTap: () => state.selectSuggestion(profile),
              ): const SizedBox(height: 0,);
            }, 
            onChipTapped: (Object? value) { 
            },
          )),
        ],
      ),
    );
  }
}
