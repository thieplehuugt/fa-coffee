import 'package:flutter/material.dart';

class IconCircle extends StatefulWidget {
  final String? text;
  final IconData icon;
  final double? radius;
  final EdgeInsets? padding;
  final Color? backgroundColor;
  final Color? color;
  final Color? textColor;
  final TextStyle? textStyle;
  const IconCircle({
    this.text,
    required this.icon,
    this.radius,
    this.padding,
    this.backgroundColor,
    this.color,
    this.textColor,
    this.textStyle,
    Key? key, 
  }): super(key: key);
  @override
  _IconCircleState createState() => _IconCircleState();
}

class _IconCircleState extends State<IconCircle> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            width: widget.radius! * 2,
            height: widget.radius! * 2,
            padding:
                widget.padding ?? const EdgeInsets.all(8),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: widget.backgroundColor ?? Theme.of(context).colorScheme.primary,
            ),
            child: Icon(widget.icon, color: widget.color,),
          ),
      );
  }
}
