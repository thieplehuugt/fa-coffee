import 'package:flutter/material.dart';

typedef OnSearchTap = Function(String);
typedef OnChange = Function(int);

class NumberInputField extends StatefulWidget {
  final int min;
  final int max;
  final Icon increaseIcon;
  final Icon decreaseIcon;
  final OnChange onChage;
  const NumberInputField({
    this.min = 0,
    this.max = 1000,
    this.decreaseIcon = const Icon(
      Icons.remove,
      color: Colors.grey,
      size: 18,
    ),
    this.increaseIcon = const Icon(
      Icons.add,
      color: Colors.grey,
      size: 18,
    ),
    required this.onChage,
    Key? key,
  }) : super(key: key);

  @override
  _NumberInputFieldState createState() => _NumberInputFieldState();
}

class _NumberInputFieldState extends State<NumberInputField> {
  int value = 0;

  @override
  void initState() {
    super.initState();
    value = widget.min;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        InkWell(
          child: widget.decreaseIcon,
          onTap: () => {
            setState(
              () {
                if (value > widget.min) {
                  value--;
                  widget.onChage(value);
                }
              },
            )
          },
        ),
        const SizedBox(
          width: 6,
        ),
        Text(value.toString()),
        const SizedBox(
          width: 6,
        ),
        InkWell(
          child: widget.increaseIcon,
          onTap: () => {
            setState(
              () {
                if (value < widget.max) {                  
                  value++;
                  widget.onChage(value);
                }
              },
            )
          },
        )
      ],
    );
  }
}
