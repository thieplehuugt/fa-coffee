import 'package:flutter/material.dart';

import 'stepper_progress_view.dart';

class StepperPage {
  final IconData icon;
  final Widget view;
  const StepperPage({required this.icon, required this.view});
}

class StepperWidget extends StatefulWidget {
  final List<StepperPage> pages;
  final VoidCallback onFinish;
  final int? activeIndex;
  const StepperWidget({required this.pages, required this.onFinish, this.activeIndex = -1,  Key? key})
      : super(key: key);

  @override
  _StepperWidgetState createState() => _StepperWidgetState();
}

class _StepperWidgetState extends State<StepperWidget> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    List<Widget> pageViews = List<Widget>.empty(growable: true);
    List<IconData> icons = List<IconData>.empty(growable: true);
    for (var page in widget.pages) {
      icons.add(page.icon);
      pageViews.add(page.view);
    }
    return Column(
      children: [
        StepProgressView(
          icons: icons,
          width: MediaQuery.of(context).size.width,
          curStep: index + 1,
          color: const Color(0xffe5649e),
        ),
        const SizedBox(
          height: 12,
        ),
        IndexedStack(
          children: pageViews,
          index: index,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            (index == 0)
                ? const SizedBox()
                : ElevatedButton.icon(
                    onPressed: () {
                      setState(() {
                        index--;
                      });
                    },
                    icon: const Icon(
                      // <-- Icon
                      Icons.arrow_back_ios,
                      size: 24.0,
                    ),
                    label: const Text('Back'), // <-- Text
                  ),
            widget.activeIndex! >= 0 && widget.activeIndex! != index-1 ? ElevatedButton(
              onPressed: () {
                if (index == widget.pages.length - 1) {
                  widget.onFinish();
                } else {
                  setState(() {
                    index++;
                  });
                }
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  (index < widget.pages.length - 1)
                      ? const Text('Next')
                      : const Text('Finish'),
                  const SizedBox(
                    width: 5,
                  ),
                  const Icon(
                    // <-- Icon
                    Icons.arrow_forward_ios,
                    size: 24.0,
                  ),
                ],
              ),
            ): ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.grey),),
              onPressed: () {                
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  (index < widget.pages.length - 1)
                      ? const Text('Next')
                      : const Text('Finish'),
                  const SizedBox(
                    width: 5,
                  ),
                  const Icon(
                    // <-- Icon
                    Icons.arrow_forward_ios,
                    size: 24.0,
                  ),
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
