import 'package:flutter/material.dart';

class SuggestionsBoxController {
  final BuildContext context;

  late OverlayEntry overlayEntry;

  bool _isOpened = false;

  bool get isOpened => _isOpened;

  SuggestionsBoxController(this.context);

  open() {
    if (_isOpened) return;
    Overlay.of(context)?.insert(overlayEntry);
    _isOpened = true;
  }

  close() {
    if (!_isOpened) return;
    overlayEntry.remove();
    _isOpened = false;
  }

  toggle() {
    if (_isOpened) {
      close();
    } else {
      open();
    }
  }
}
