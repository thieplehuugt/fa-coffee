import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import 'cell_decoration.dart';
typedef OnDateTimeSelect = Function(DateTime datetime);
SfDateRangePicker getDatePicker(
    {
      List<DateTime>? specialDates,
      List<DateTime>? blackoutDates,
      ThemeData? theme,
      required OnDateTimeSelect onSelectTime,
    }) {
  final bool isDark = theme != null && theme.brightness == Brightness.dark;

  final Color monthCellBackground =
      isDark ? const Color(0xFF232731) : const Color(0xfff7f4ff);
  final Color indicatorColor =
      isDark ? const Color(0xFF5CFFB7) : const Color(0xFF1AC4C7);
  final Color highlightColor =
      isDark ? const Color(0xFF5CFFB7) : Colors.deepPurpleAccent;
  final Color cellTextColor =
      isDark ? const Color(0xFFDFD4FF) : const Color(0xFF130438);

  return SfDateRangePicker(
    selectionShape: DateRangePickerSelectionShape.rectangle,
    selectionColor: highlightColor,
    selectionTextStyle:
        TextStyle(color: isDark ? Colors.black : Colors.white, fontSize: 14),
    minDate: DateTime.now(),
    maxDate: DateTime.now().add(const Duration(days: 500)),
    headerStyle: DateRangePickerHeaderStyle(
        textAlign: TextAlign.center,
        textStyle: TextStyle(
          fontSize: 18,
          color: cellTextColor,
        )),
    monthCellStyle: DateRangePickerMonthCellStyle(
        cellDecoration: CellDecoration(
            backgroundColor: monthCellBackground,
            showIndicator: false,
            indicatorColor: indicatorColor),
        todayCellDecoration: CellDecoration(
            borderColor: highlightColor,
            backgroundColor: monthCellBackground,
            showIndicator: false,
            indicatorColor: indicatorColor),
        specialDatesDecoration: CellDecoration(
            backgroundColor: monthCellBackground,
            showIndicator: true,
            indicatorColor: indicatorColor),
        disabledDatesTextStyle: TextStyle(
          color: isDark ? const Color(0xFF666479) : const Color(0xffe2d7fe),
        ),
        weekendTextStyle: TextStyle(
          color: highlightColor,
        ),
        textStyle: TextStyle(color: cellTextColor, fontSize: 14),
        specialDatesTextStyle: TextStyle(color: cellTextColor, fontSize: 14),
        todayTextStyle: TextStyle(color: highlightColor, fontSize: 14)),
    yearCellStyle: DateRangePickerYearCellStyle(
      todayTextStyle: TextStyle(color: highlightColor, fontSize: 14),
      textStyle: TextStyle(color: cellTextColor, fontSize: 14),
      disabledDatesTextStyle: TextStyle(
          color: isDark ? const Color(0xFF666479) : const Color(0xffe2d7fe)),
      leadingDatesTextStyle:
          TextStyle(color: cellTextColor.withOpacity(0.5), fontSize: 14),
    ),
    showNavigationArrow: true,
    todayHighlightColor: highlightColor,
    monthViewSettings: DateRangePickerMonthViewSettings(
      firstDayOfWeek: 1,
      viewHeaderStyle: DateRangePickerViewHeaderStyle(
          textStyle: TextStyle(
              fontSize: 10, color: cellTextColor, fontWeight: FontWeight.w600)),
      dayFormat: 'EEE',
      specialDates: specialDates,
      blackoutDates: blackoutDates,
    ),
    onSelectionChanged: (args)=>{
      onSelectTime(args.value)
    },
  );
}
