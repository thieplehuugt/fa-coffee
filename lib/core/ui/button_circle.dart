import 'package:flutter/material.dart';

typedef OnButtonPress = Function();

class ButtonCircle extends StatelessWidget {
  final String? text;
  final Widget? icon;
  final double radius;
  final EdgeInsets? padding;
  final Color backgroundColor;
  final Color color;
  final Color textColor;
  final TextStyle textStyle;
  final OnButtonPress onPressed;
  const ButtonCircle({
    this.text,
    this.icon,
    this.radius = 0.0,
    this.padding,
    this.backgroundColor = Colors.white,
    this.color = Colors.black,
    this.textColor = Colors.black,
    this.textStyle = const TextStyle(color: Colors.black),
    required this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onPressed();
      },
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: radius * 2,
              height: radius * 2,
              padding: padding ?? const EdgeInsets.all(8),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: backgroundColor,
              ),
              child: icon ??
                  const SizedBox(
                    height: 0,
                  ),
            ),
            const SizedBox(
              height: 4,
            ),
            text != null
                ? Text(text!,
                    style: TextStyle(fontSize: 12, color: textColor),
                    textAlign: TextAlign.center)
                : const SizedBox(
                    height: 0,
                    width: 0,
                  ),
          ],
        ),
      ),
    );
  }
}

