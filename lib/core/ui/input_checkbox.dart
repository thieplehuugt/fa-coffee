import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CheckBoxInput extends StatelessWidget {
  const CheckBoxInput({Key? key, this.value = false, this.title, this.color, required this.onChanged})
      : super(key: key);
  final bool value;
  final String? title;
  final Color? color;
  final ValueChanged<bool> onChanged;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onChanged(value);
      },
      child: Row(
        children: [
          value
              ? FaIcon(
                  FontAwesomeIcons.check,
                  color: color ?? Theme.of(context).colorScheme.primary,
                  size: 20,
                )
              : Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.transparent,
                    border: Border.all(
                      color: color ?? Theme.of(context).colorScheme.primary,
                      width: 1.5,
                    ),
                  ),
                  child: const Icon(null),
                ),
          const SizedBox(
            width: 8,
          ),
          Text(
            title??"",
            style: const TextStyle().copyWith(
              color: color ?? Theme.of(context).colorScheme.primary,
            ),
          ),
        ],
      ),
    );
  }
}
