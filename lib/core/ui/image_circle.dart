import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CircleImage extends StatelessWidget {
  const CircleImage(
      {Key? key,
      this.url,
      this.assetPath,
      this.padding = 2,
      this.width = 0,
      this.icon,
      this.color})
      : super(key: key);
  final String? url;
  final String? assetPath;
  final double? padding;
  final double? width;
  final Color? color;
  final Icon? icon;

  @override
  Widget build(BuildContext context) {
    Widget image;
    if (url == null || url == "") {
      image = Image.asset(
        assetPath ?? "assets/images/default-picture.png",
        fit: BoxFit.cover,
      );
    } else {
      image = CachedNetworkImage(
        fit: BoxFit.cover,
        imageUrl: url!,
        progressIndicatorBuilder: (context, url, downloadProgress) =>
            CircularProgressIndicator(value: downloadProgress.progress),
        errorWidget: (context, url, error) => const Icon(Icons.error),
      );
    }

    double r = MediaQuery.of(context).size.width * 0.25;
    if (width != null) {
      r = width!;
    }

    return CircleAvatar(
      radius: r * 0.5 + (padding != null ? padding! : 0),
      backgroundColor: color ?? Theme.of(context).primaryColor,
      child: ClipOval(
        child: Container(
          width: r,
          height: r,
          child: icon ?? image,
          color: Colors.white,
        ),
      ),
    );
  }
}
