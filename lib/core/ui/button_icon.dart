import 'package:flutter/material.dart';

typedef OnButtonPress = Function();

class ButtonIcon extends StatefulWidget {
  final String? text;
  final IconData icon;
  final double radius;
  final EdgeInsets padding;
  final Color backgroundColor;
  final Color color;
  final Color textColor;
  final TextStyle textStyle;
  final OnButtonPress onPressed;
  const ButtonIcon({
    this.text,
    required this.icon,
    this.radius = 0.0,
    this.padding = EdgeInsets.zero,
    this.backgroundColor = Colors.white,
    this.color = Colors.black,
    this.textColor = Colors.black,
    this.textStyle = const TextStyle(color: Colors.black),
    required this.onPressed,
    Key? key,
  }) : super(key: key);
  @override
  _ButtonIconState createState() => _ButtonIconState();
}

class _ButtonIconState extends State<ButtonIcon> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        widget.onPressed();
      },
      child: Container(
        padding: const EdgeInsets.only(left: 16, right: 16),
        decoration: BoxDecoration(
          color: widget.backgroundColor,
          borderRadius: const BorderRadius.only(topRight: Radius.circular(24), bottomRight: Radius.circular(24))
        ),
        child: Center(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
                widget.icon,
                color: widget.color,
              ),
            widget.text != null ? Text(
              widget.text!,
              style: TextStyle(fontSize: 14, color: widget.textColor, fontWeight: FontWeight.bold),
            ) : const SizedBox(height: 0,),
          ],
        ),
      ),
      ),
    );
  }
}
