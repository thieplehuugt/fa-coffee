import 'package:flutter/material.dart';

import 'button_icon.dart';

typedef OnFieldSubmitted = Function(dynamic);

class TextInputField extends StatefulWidget {
  final TextEditingController controller;
  final String hintText;
  final EdgeInsets contentPadding;
  final Color borderColor;
  final double borderRadius;
  final Color backgroundColor;
  final Color textColor;
  final InputDecoration? decoration;
  final TextInputType keyboardType;
  final OnFieldSubmitted? onFieldSubmitted;
  final TextInputAction textInputAction;
  final bool enabled;
  final bool obscureText;
  final int maxLength;
  final FocusNode? focusNode;
  const TextInputField(
      {required this.controller,
      this.keyboardType = TextInputType.text,
      this.onFieldSubmitted,
      this.textInputAction = TextInputAction.none,
      this.decoration,
      this.hintText = "Hint Text",
      this.contentPadding = const EdgeInsets.all(8),
      this.borderColor = Colors.grey,
      this.borderRadius = 8,
      this.backgroundColor = Colors.white,
      this.textColor = Colors.black,
      this.enabled = true,
      this.obscureText = false,
      this.maxLength = 255,
      this.focusNode,
      Key? key})
      : super(key: key);
  @override
  _TextInputFieldState createState() => _TextInputFieldState();
}

class _TextInputFieldState extends State<TextInputField> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      obscureText: widget.obscureText,
      decoration: widget.decoration ??
          InputDecoration(
            hintText: widget.hintText,
            enabled: widget.enabled,
            hintStyle: TextStyle(color: widget.textColor),
            isDense: true,
            fillColor: widget.backgroundColor,
            filled: true,
            enabledBorder: OutlineInputBorder(
              gapPadding: 0,
              borderSide: BorderSide(
                width: 1.5,
                color: widget.borderColor,
              ),
              borderRadius:
                  BorderRadius.all(Radius.circular(widget.borderRadius)),
            ),
            focusedBorder: OutlineInputBorder(
              gapPadding: 0,
              borderSide: BorderSide(
                width: 1.5,
                color: widget.borderColor,
              ),
              borderRadius:
                  BorderRadius.all(Radius.circular(widget.borderRadius)),
            ),
            disabledBorder: OutlineInputBorder(
              gapPadding: 0,
              borderSide: BorderSide(
                width: 1.5,
                color: widget.borderColor,
              ),
              borderRadius:
                  BorderRadius.all(Radius.circular(widget.borderRadius)),
            ),
            contentPadding: widget.contentPadding,
          ),
      style: TextStyle(color: widget.textColor),
      keyboardType: widget.keyboardType,
      focusNode: widget.focusNode,
      onFieldSubmitted: (value) {
        if (widget.onFieldSubmitted != null) {
          widget.onFieldSubmitted!(value);
        }
      },
      textInputAction: widget.textInputAction,
    );
  }
}

typedef OnSendPress = Function();

class InputSendMessageField extends StatefulWidget {
  final TextEditingController controller;
  final String hintText;
  final EdgeInsets contentPadding;
  final Color borderColor;
  final BorderRadius borderRadius;
  final Color backgroundColor;
  final Color textColor;
  final InputDecoration? decoration;
  final OnSendPress onSendPress;
  const InputSendMessageField(
    {
      required this.controller,
      this.decoration,
      this.hintText = "Hint Text",
      this.contentPadding = const EdgeInsets.all(8),
      this.borderColor = Colors.grey,
      this.borderRadius = BorderRadius.zero,
      this.backgroundColor = Colors.white,
      this.textColor = Colors.black,
      required this.onSendPress,
      Key? key,
    }
  ) : super(key: key);
  @override
  _InputSendMessageFieldState createState() => _InputSendMessageFieldState();
}

class _InputSendMessageFieldState extends State<InputSendMessageField> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(left: 12),
              decoration: BoxDecoration(
                border: Border.all(
                    color: Theme.of(context).colorScheme.primary, width: 1),
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(24),
                    bottomLeft: Radius.circular(24)),
              ),
              child: TextFormField(
                  controller: widget.controller,
                  textAlignVertical: TextAlignVertical.center,
                  decoration: const InputDecoration(
                    isDense: true,
                    border: InputBorder.none,
                  ),
                  style: TextStyle(
                      color: Theme.of(context).colorScheme.onSecondary),
                  textInputAction: TextInputAction.send,
                  onFieldSubmitted: (term) {
                    widget.onSendPress();
                  }),
            ),
          ),
          ButtonIcon(
            icon: Icons.near_me,
            backgroundColor: Theme.of(context).colorScheme.primary,
            textColor: Colors.white,
            color: Colors.white,
            onPressed: () => widget.onSendPress(),
          ),
        ],
      ),
    );
  }
}
