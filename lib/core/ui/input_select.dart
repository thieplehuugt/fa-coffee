import 'package:flutter/material.dart';

typedef OnChanged = Function(dynamic);
class Item {
  String key;
  String value;

  Item({
    this.key = '',
    this.value = '',
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        key: json["key"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
      };
}
class InputSelectFiled<T> extends StatefulWidget {
  const InputSelectFiled({
    Key? key,
    required this.value,
    required this.items,
    required this.controller,
    required this.onChanged,
    this.showValue = 'value',
    this.hintText = "Hint Text",
    this.contentPadding = const EdgeInsets.all(0),
    this.borderColor = Colors.grey,
    this.borderRadius = const BorderRadius.all(Radius.zero),
    this.backgroundColor = Colors.white,
    this.textColor = Colors.black,
    this.decoration = const InputDecoration(
          fillColor: Colors.white,
          border: InputBorder.none,
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              borderSide: BorderSide(color: Colors.blue)),
        ),
    this.enabled = true,
  }) : super(key: key);
  final T value;
  final List<T> items;
  final TextEditingController controller;
  final OnChanged onChanged;
  final String showValue;

  final String hintText;
  final EdgeInsets contentPadding;
  final Color borderColor;
  final BorderRadius borderRadius;
  final Color backgroundColor;
  final Color textColor;
  final InputDecoration decoration;
  final bool enabled;

  @override
  _InputSelectFiledState createState() => _InputSelectFiledState<T>();
}

class _InputSelectFiledState<T> extends State<InputSelectFiled<T>> {
  dynamic _value;
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<T>(
      value: widget.value,
      decoration: InputDecoration(
              hintText: widget.hintText,
              enabled: widget.enabled,
              hintStyle: TextStyle(
                  color: widget.textColor,
                  fontSize: 16),
              isDense: true,
              fillColor: widget.backgroundColor,
              filled: true,
              enabledBorder: OutlineInputBorder(
                gapPadding: 0,
                borderSide: BorderSide(
                  width: 1.5,
                  color: widget.borderColor,
                ),
                borderRadius: widget.borderRadius,
              ),
              focusedBorder: OutlineInputBorder(
                gapPadding: 0,
                borderSide: BorderSide(
                  width: 1.5,
                  color: widget.borderColor
                ),
                borderRadius: widget.borderRadius,
              ),
              disabledBorder: OutlineInputBorder(
                gapPadding: 0,
                borderSide: BorderSide(
                  width: 1.5,
                  color: widget.borderColor,
                ),
                borderRadius: widget.borderRadius,
              ),
              contentPadding: widget.contentPadding,
            ),
      style: TextStyle(
          color: widget.textColor,
          fontSize: 16),
          
      onChanged: (T? newValue) {
        if(widget.onChanged!=null){
          widget.onChanged(newValue);
        }else{
          setState(() {
              _value = newValue;
              widget.controller.text = _value;
            });
        }
        FocusScope.of(context).requestFocus(FocusNode());
      } ,    
      items: widget.items.map((value) {
        var _value;
        if (value is Item) {
          _value = value.value;
          if (widget.showValue == 'key') _value = value.key;
        } else {
          _value = value;
        }
        return DropdownMenuItem<T>(
          value: value,
          child: Text(
              //value.toString(),
              '$_value'),
        );
      }).toList(),
    );
  }
}
