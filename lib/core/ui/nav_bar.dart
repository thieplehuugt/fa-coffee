import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../about/index.dart';
import '../../blocs/network/network_bloc.dart';

class NavBar {
  PreferredSizeWidget? build(
      {required BuildContext context,
      String? title,
      bool? leading,
      bool? showMenu = true,
      Color? color,
      TextStyle? textStyle}) {
    return AppBar(
      backgroundColor: color ?? Theme.of(context).colorScheme.onBackground,
      bottom: PreferredSize(
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Column(
            children: [
              Row(
                children: [
                  leading == true
                      ? InkWell(
                          child: Icon(
                            Icons.arrow_back,
                            color: Theme.of(context).colorScheme.onPrimary,
                            size: 24,
                          ),
                          onTap: () => Navigator.of(context).pop(),
                        )
                      : const SizedBox(),
                  const Expanded(child: Text("")),
                  Padding(
                    padding: const EdgeInsets.all(4),
                    child: Stack(
                      children: [
                        Image.asset(
                          "assets/images/comment.png",
                          height: 24,
                          width: 24,
                        ),
                        SizedBox(
                          height: 24,
                          width: 24,
                          child: Center(
                            child: Text(
                              "30",
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).colorScheme.primary),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    child: Padding(
                        padding: const EdgeInsets.all(4),
                        child: Image.asset(
                          "assets/images/question-cricle.png",
                          height: 22,
                          width: 22,
                        )),
                    onTap: title == null
                        ? null
                        : () => Navigator.of(context).push(
                              PageRouteBuilder(
                                // builder: (context) {
                                //   return AboutPage(title: title,);
                                // },
                                fullscreenDialog: true,
                                opaque: false,
                                pageBuilder: (BuildContext context,
                                    Animation<double> animation,
                                    Animation<double> secondaryAnimation) {
                                  return AboutPage(
                                    title: title,
                                  );
                                },
                              ),
                            ),
                  ),
                  InkWell(
                    child: const Padding(
                        padding: EdgeInsets.only(left: 8, top: 4, bottom: 4),
                        child: FaIcon(
                          FontAwesomeIcons.signHanging,
                          color: Colors.white,
                          size: 24,
                        )),
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          // return object of type Dialog
                          return AlertDialog(
                            title: const Text("お知らせ"),
                            content: const Text("ログアウトしますか？"),
                            actions: <Widget>[
                              FlatButton(
                                child: const Text("はい"),
                                color: const Color(0xffaa66cc),
                                onPressed: () {},
                              ),
                              FlatButton(
                                child: const Text("いいえ"),
                                color: const Color(0xff4285f4),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                  ),
                ],
              ),
              title != null
                  ? Text(
                      title,
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    )
                  : const SizedBox(
                      height: 0,
                    ),
              Container(
                color: color ?? Theme.of(context).colorScheme.primary,
                height: 12,
              ),
              BlocBuilder<NetworkBloc, NetworkState>(
                  bloc: BlocProvider.of<NetworkBloc>(context),
                  builder: (context, state) {
                    if (state is NetworkConnectedState || state is NetworkInitial) {
                      return const SizedBox(height: 0,);
                    } else {
                      return const Text(
                        'Unconnected',
                      );
                    }
                  })
            ],
          ),
        ),
        preferredSize: const Size(0, 0),
      ),
    );
  }
}
