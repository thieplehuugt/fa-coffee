import 'package:flutter/material.dart';

import '../../constants.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  int isOn = 0;
  @override
  Widget build(BuildContext context) {
    bool _darkModeEnabled =
        Theme.of(context).brightness == Brightness.dark;
        _darkModeEnabled ?isOn=1 : isOn =0;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          margin: const EdgeInsets.only(top: verticalPadding, left: verticalPadding, right: verticalPadding),
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color(0xfffbb448), Color(0xffe46b10)])),
          child: Center(
            child: TextButton(
                  style: TextButton.styleFrom(
                    padding: const EdgeInsets.all(16.0),
                    primary: Colors.white,
                    textStyle: const TextStyle(fontSize: 20),
                  ),
                  onPressed: (){

                  },
                  child: const Text('Active Darkmode'),
                ),
          ),
        ),
      ),
    );
  }
}
