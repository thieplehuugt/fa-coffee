import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/main/main_bloc.dart';
import '../../components/trip_card.dart';
import '../../constants.dart';
import '../../models/trip.dart';
import 'trip/trip.dart';

class MyToursScreen extends StatefulWidget {
  const MyToursScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _MyToursScreenScreenState createState() => _MyToursScreenScreenState();
}

class _MyToursScreenScreenState extends State<MyToursScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainBloc, MainState>(
        bloc: BlocProvider.of<MainBloc>(context),
        builder: (context, state) {
          if (state is ReadyState) {
            var trips = state.myTrips;
            return Padding(
                padding: const EdgeInsets.only(
                  left: horizontalPadding,
                  right: horizontalPadding,
                ),
                child: ListView.builder(
                  itemCount: trips.length,
                  padding: const EdgeInsets.only(top: 0.0),
                  itemBuilder: (context, index) {
                    return TripCard(
                      trip: trips[index],
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TripPage(
                                    trip: trips[index],
                                  )),
                        );
                      },
                    );
                  },
                ));
          }
          return const SizedBox();
        });
  }
}
