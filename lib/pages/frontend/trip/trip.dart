import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:draggable_home/draggable_home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:timeline_tile/timeline_tile.dart';
import '../../../components/photo_slider.dart';
import '../../../constants.dart';
import '../../../core/ui/button_circle.dart';
import '../../../core/ui/icon_indicator.dart';
import '../../../models/tab_index.dart';
import '../../../models/trip.dart';
import 'cubit/trip_cubit.dart';

class TripPage extends StatelessWidget {
  final Trip trip;
  const TripPage({Key? key, required this.trip}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => TripCubit(),
      child: DraggableHome(
        leading: InkWell(
          child: const Icon(Icons.arrow_back_ios, color: Colors.white),
          onTap: () => Navigator.pop(context),
        ),
        title: Text(trip.title),
        actions: [
          IconButton(
              onPressed: () => Navigator.pop(context),
              icon: const Icon(Icons.settings, color: Colors.white)),
        ],
        headerWidget: header(context),
        headerBottomBar: headerBottomBarWidget(),
        curvedBodyRadius: 0,
        body: [
          TripDetailView(trip: trip),
        ],
        fullyStretchable: true,
        backgroundColor: Colors.white,
        appBarColor: Theme.of(context).colorScheme.primary,
        floatingActionButton: FloatingActionButton(
          child: const Icon(
            Icons.add_shopping_cart_sharp,
            size: 32,
            color: Colors.white,
          ),
          onPressed: () => {
            showMaterialModalBottomSheet(
              context: context,
              backgroundColor: Colors.white,
              builder: (context) => SingleChildScrollView(
                controller: ModalScrollController.of(context),
                child: Padding(
                  padding: const EdgeInsets.all(12),
                  child: Container(),
                ),
              ),
            )
          },
          backgroundColor: Theme.of(context).colorScheme.primary,
        ),
      ),
    );
  }

  Widget headerBottomBarWidget() {
    return const SizedBox();
  }

  Widget header(BuildContext context) {
    return Stack(
      children: <Widget>[
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.4,
          width: MediaQuery.of(context).size.width,
          child: CachedNetworkImage(
            imageUrl: trip.thumbnail!,
            fit: BoxFit.cover,
            placeholder: (context, url) => const CircularProgressIndicator(),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: const EdgeInsets.all(20.0),
          width: MediaQuery.of(context).size.width,
          decoration:
              const BoxDecoration(color: Color.fromRGBO(58, 66, 86, .5)),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(height: 130.0),
                Text(
                  trip.title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(color: Colors.white, fontSize: 24.0),
                ),
                const SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            Icon(
                              Icons.star,
                              color: Theme.of(context).colorScheme.primary.withGreen(255),
                            ),
                            Icon(
                              Icons.star,
                              color: Theme.of(context).colorScheme.primary.withGreen(255),
                            ),
                            Icon(
                              Icons.star,
                              color: Theme.of(context).colorScheme.primary.withGreen(255),
                            ),
                            Icon(
                              Icons.star,
                              color: Theme.of(context).colorScheme.primary.withGreen(255),
                            ),
                            Icon(
                              Icons.star,
                              color: Theme.of(context).colorScheme.primary.withGreen(255),
                            ),
                            const Text(
                              "5.0",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        )),
                  ],
                ),
                const SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(5.0)),
                      child: Text(
                        // "\$20",
                        "From \$" + trip.price.minPrice.toString(),
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(5.0)),
                      child: TextButton(
                        child: const Text(
                          "Photo",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () => {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TripPhotoSlider(
                                      trip: trip,
                                    )),
                          )
                        },
                        style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            alignment: Alignment.center),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(Icons.arrow_back, color: Colors.white),
          ),
        )
      ],
    );
  }
}

class TripDetailView extends StatefulWidget {
  final Trip trip;
  const TripDetailView({required this.trip, Key? key}) : super(key: key);
  @override
  _TripDetailViewState createState() => _TripDetailViewState();
}

class _TripDetailViewState extends State<TripDetailView> {
  TextEditingController adultAmountController = TextEditingController();
  TextEditingController childAmountController = TextEditingController();
  final Completer<GoogleMapController> _controller = Completer();
  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> tabItems = List<Widget>.empty(growable: true);
    List<Widget> tabContents = List<Widget>.empty(growable: true);
    final selectedTab = context.select((TripCubit cubit) => cubit.state.tab);
    for (var tab in widget.trip.tabs) {
      final re = RegExp(r'/\&*');
      //var name = tab.name.replaceAll(re, "\r\n");
      var name = tab.name.replaceAll('/','\r\n').replaceAll('&', '\r\n');
      tabItems.add(ButtonCircle(
        icon: tab.icon != null ? Icon(tab.icon) : const Icon(Icons.abc),
        text: name,
        radius: 24,
        backgroundColor: selectedTab == tab.index
            ? Theme.of(context).colorScheme.onBackground
            : Theme.of(context).colorScheme.primary,
        onPressed: () => context.read<TripCubit>().setTab(tab.index),
      ));
      List<Widget> sections = List<Widget>.empty(growable: true);
      for (var i = 0; i < tab.sections.length; i++) {
        var section = tab.sections[i];
        if (tab.index == TabIndex.itinerary) {
          sections.add(Column(children: [
            TimelineTile(
              axis: TimelineAxis.vertical,
              alignment: TimelineAlign.start,
              isFirst: i == 0 ? true : false,
              isLast: i == tab.sections.length - 1 ? true : false,
              indicatorStyle: IndicatorStyle(
                width: 20,
                color: Theme.of(context).colorScheme.primary,
                indicatorXY: 0.0,
                padding: const EdgeInsets.all(0),
              ),
              endChild: Padding(
                padding: const EdgeInsets.only(left: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                  Text(
                    section.title!,
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  Text(section.content),
                  const SizedBox(height: 12,),
                ]),
              ),
            ),
          ]));
        } else if (tab.index == TabIndex.map) {
          sections.add(SizedBox(
            height: 300,
            width: 500,
            child: GoogleMap(
              mapType: MapType.hybrid,
              initialCameraPosition: _kGooglePlex,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
          ));
        } else {
          sections.add(Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              section.title != null
                  ? Text(
                      section.title!,
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium
                          ?.copyWith(fontWeight: FontWeight.bold),
                    )
                  : const SizedBox(
                      height: 0,
                    ),
              Text(section.content),
              const SizedBox(
                height: 12,
              )
            ],
          ));
        }
      }
      tabContents.add(Column(
        children: sections,
      ));
    }
    return Padding(
        padding: const EdgeInsets.all(verticalPadding),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: tabItems,
            ),
            const SizedBox(
              height: verticalPadding,
            ),
            IndexedStack(
              index: selectedTab.index,
              children: tabContents,
            ),
          ],
        ));
  }

  TimelineTile _buildTimelineTile({
    required IconIndicator indicator,
    required String hour,
    required String weather,
    required String temperature,
    required String phrase,
    bool isLast = false,
  }) {
    return TimelineTile(
      alignment: TimelineAlign.manual,
      lineXY: 0.3,
      beforeLineStyle: LineStyle(color: Colors.white.withOpacity(0.7)),
      indicatorStyle: IndicatorStyle(
        indicatorXY: 0.3,
        drawGap: true,
        width: 30,
        height: 30,
        indicator: indicator,
      ),
      isLast: isLast,
      startChild: Center(
        child: Container(
          alignment: const Alignment(0.0, -0.50),
          child: Text(
            hour,
            style: GoogleFonts.lato(
              fontSize: 18,
              color: Colors.white.withOpacity(0.6),
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
      endChild: Padding(
        padding:
            const EdgeInsets.only(left: 16, right: 10, top: 10, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              weather,
              style: GoogleFonts.lato(
                fontSize: 18,
                color: Colors.white.withOpacity(0.8),
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 4),
            Text(
              temperature,
              style: GoogleFonts.lato(
                fontSize: 16,
                color: Colors.white.withOpacity(0.8),
                fontWeight: FontWeight.normal,
              ),
            ),
            const SizedBox(height: 4),
            Text(
              phrase,
              style: GoogleFonts.lato(
                fontSize: 14,
                color: Colors.white.withOpacity(0.6),
                fontWeight: FontWeight.normal,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class TripPhotoSlider extends StatelessWidget {
  final Trip trip;
  const TripPhotoSlider({required this.trip, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (trip.images == null || trip.images!.isEmpty) {
      return const SizedBox();
    }
    List<String> images = List<String>.empty(growable: true);
    for (var media in trip.images!) {
      images.add(media.url);
    }
    return Material(
      color: Colors.black,
      child: Stack(
        children: [
          PhotoSlider(photos: images),
          Container(
            margin: const EdgeInsets.only(top: 60),
            child: InkWell(
              child: const Icon(Icons.arrow_back),
              onTap: () => Navigator.pop(context),
            ),
          ),
        ],
      ),
    );
  }
}
