part of 'trip_cubit.dart';


class TripState extends Equatable {
  const TripState({
    this.tab = TabIndex.overview,
  });

  final TabIndex tab;

  @override
  List<Object> get props => [tab];
}
