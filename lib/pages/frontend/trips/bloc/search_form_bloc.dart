import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'search_form_event.dart';
part 'search_form_state.dart';

class SearchFormBloc extends Bloc<ScreenEvent, ScreenState> {
  SearchFormBloc() : super(SearchFormInitial()) {
    on<OpenSearchAdvancedEvent>((event, emit) => emit(OpenAdvanced()));
    on<CloseSearchAdvancedEvent>((event, emit) => emit(CloseAdvanced()));
  }
}
