part of 'search_form_bloc.dart';

abstract class ScreenState extends Equatable {
  const ScreenState();
  
  @override
  List<Object> get props => [];
}

class SearchFormInitial extends ScreenState {}

class OpenAdvanced extends ScreenState {}
class CloseAdvanced extends ScreenState {}