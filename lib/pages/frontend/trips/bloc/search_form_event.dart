part of 'search_form_bloc.dart';

abstract class ScreenEvent extends Equatable {
  const ScreenEvent();

  @override
  List<Object> get props => [];
}

class OpenSearchAdvancedEvent extends ScreenEvent {}
class CloseSearchAdvancedEvent extends ScreenEvent {}