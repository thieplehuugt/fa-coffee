import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/main/main_bloc.dart';
import '../../../components/trip_card.dart';
import '../../../core/ui/input_location.dart';
import '../../../core/ui/input_search.dart';
import '../../../core/ui/nav_bar.dart';
import '../trip/trip.dart';
import 'bloc/search_form_bloc.dart';

class TripsPage extends StatefulWidget {
  const TripsPage({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _TripsPageState createState() => _TripsPageState();
}

class _TripsPageState extends State<TripsPage> {

  double _lowerValue = 10;
  double _upperValue = 200;
  TextEditingController controller = TextEditingController();
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: NavBar().build(context: context, leading: true),
        body: BlocProvider<SearchFormBloc>(
      create: (BuildContext context) =>
          SearchFormBloc()..add(CloseSearchAdvancedEvent()),
      child: Column(
        children: [
          Container(
            width: width,
            color: Theme.of(context).colorScheme.primary,
            padding: const EdgeInsets.only(left: 32, right: 32, bottom: 12),
            child: InputSearchField(
              borderRadius: const Radius.circular(16.0),
              iconSize: const Size(35, 35),
              controller: controller,
              hintText: "Search Trips",
              showIndicator: true,
              onSearchTap: (text) => {},
              onChange: (string) {},
            ),
          ),
          Expanded(
            child: SizedBox(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 16,
                    width: width,
                  ),
                  _buildAdvanceSearchTitle(),
                  Expanded(
                      child: Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 12, right: 12),
                        child: SizedBox(
                        width: width,
                        child: BlocBuilder<MainBloc, MainState>(
                            bloc: BlocProvider.of<MainBloc>(context),
                            builder: (context, state) {
                              if (state is ReadyState) {
                                var trips = state.trips;
                                return ListView.builder(
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return TripCard(
                                      trip: trips[index],
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => TripPage(
                                                    trip: trips[index],
                                                  )),
                                        );
                                      },
                                    );
                                  },
                                  controller: _scrollController,
                                );
                              }
                              return const SizedBox(
                                height: 0,
                              );
                            }),
                      ),),
                      BlocBuilder<SearchFormBloc, ScreenState>(
                          builder: (context, state) {
                        if (state is OpenAdvanced) {
                          return AnimatedSwitcher(
                            duration: const Duration(milliseconds: 250),
                            child: _buildAdvancedSearchForm(),
                          );
                        }
                        return const AnimatedSwitcher(
                          duration: Duration(milliseconds: 250),
                          child: SizedBox(
                            height: 0,
                          ),
                        );
                      }),
                    ],
                  )),
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }

  Widget _buildAdvanceSearchTitle() {
    double width = MediaQuery.of(context).size.width;
    return BlocBuilder<SearchFormBloc, ScreenState>(builder: (context, state) {
      if (state is OpenAdvanced) {
        return Container(
          height: 32,
          width: width,
          padding: const EdgeInsets.only(left: 12, right: 12),
          color: Theme.of(context).colorScheme.primary,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              IconButton(
                icon: const Icon(
                  Icons.filter_list,
                  color: Colors.white,
                  size: 24,
                ),
                onPressed: () {
                  BlocProvider.of<SearchFormBloc>(context)
                      .add(CloseSearchAdvancedEvent());
                },
              ),
            ],
          ),
        );
      } else if (state is CloseAdvanced) {
        return Container(
          height: 32,
          width: width,
          padding: const EdgeInsets.only(left: 12, right: 12),
          color: Theme.of(context).colorScheme.background,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("",
                  style: TextStyle(
                      color: Theme.of(context).colorScheme.onBackground,
                      fontSize: 16)),
              IconButton(
                icon: Icon(
                  Icons.filter_list,
                  color: Theme.of(context).colorScheme.onBackground,
                  size: 24,
                ),
                onPressed: () {
                  BlocProvider.of<SearchFormBloc>(context)
                      .add(OpenSearchAdvancedEvent());
                },
              ),
            ],
          ),
        );
      }
      return Container();
    });
  }

  Widget _buildAdvancedSearchForm() {
    return BlocBuilder<MainBloc, MainState>(
        bloc: BlocProvider.of<MainBloc>(context),
        builder: (context, state) {
          if (state is ReadyState) {
            return Container(
              color: Theme.of(context).colorScheme.primary,
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 64, right: 64, top: 24, bottom: 24),
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    LocationMultiSelectInputField(
                      items: state.locations,
                      contentPadding: const EdgeInsets.all(0),
                      borderRadius: const BorderRadius.all(Radius.circular(8)),
                      onItemSelected: (locations) {

                      },
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Budget",
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        Text(
                            _lowerValue.toInt().toString() +
                                " USD～" +
                                _upperValue.toInt().toString() +
                                " USD",
                            style: const TextStyle(
                                color: Colors.white, fontSize: 14)),
                      ],
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    SliderTheme(
                      data: SliderTheme.of(context).copyWith(
                        activeTrackColor: Colors.white,
                        thumbColor: Colors.yellow,
                        inactiveTrackColor: Colors.deepOrangeAccent,
                        thumbShape: const RoundSliderThumbShape(
                            enabledThumbRadius: 15.0),
                        overlayShape:
                            const RoundSliderOverlayShape(overlayRadius: 30.0),
                      ),
                      child: RangeSlider(
                        values: RangeValues(_lowerValue, _upperValue),
                        max: _upperValue,
                        divisions: _upperValue.round(),
                        labels: RangeLabels(
                          _lowerValue.round().toString(),
                          _upperValue.round().toString(),
                        ),
                        onChanged: (RangeValues values) {
                          setState(() {
                            _lowerValue = values.start;
                            _upperValue = values.end;
                          });
                        },
                      ),
                    ),
                    /*
                    SfRangeSlider(
                      min: _lowerValue,
                      max: _upperValue,
                      values: SfRangeValues(_lowerValue, _upperValue),
                      interval: 20,
                      showLabels: true,
                      enableTooltip: true,
                      minorTicksPerInterval: 1,
                      onChanged: (SfRangeValues values) {
                        setState(() {
                          _lowerValue = values.start;
                          _upperValue = values.end;
                        });
                      },
                    ),            
                    */
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        OutlinedButton(
                          child: const Text("Search"),
                          onPressed: () {},
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Container();
          }
        });
  }
}
