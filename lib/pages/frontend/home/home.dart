import 'package:flutter/material.dart';
import '../../../components/destinations_card.dart';
import '../../../components/trips_card.dart';
import '../../../constants.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.only(left: horizontalPadding, right: horizontalPadding),
    child: SingleChildScrollView(child: Column(
          children: const [
            DestinationsCard(),
            SizedBox(height: verticalPadding,),
            TripsCard(),
          ],
        ),
      ),);
  }
}
