import 'package:flutter/material.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';

class AppPage {
  final Widget child;
  final Widget icon;
  final String title;
  AppPage({required this.child, required this.icon, required this.title});
}

class MainLayout extends StatefulWidget {
  final List<AppPage> pages;
  const MainLayout({required this.pages, Key? key}) : super(key: key);

  @override
  _MainLayoutState createState() => _MainLayoutState();
}

class _MainLayoutState extends State<MainLayout> with TickerProviderStateMixin {
  late TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: widget.pages.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = List.empty(growable: true);
    List<Tab> tabs = List.empty(growable: true);
    for (var item in widget.pages) {
      widgets.add(item.child);
      tabs.add(Tab(
        icon: item.icon,
      ));
    }
    var statusBarHeight = MediaQuery.of(context).viewPadding.top;
    return Container(
        decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/background.jpg"),
                  fit: BoxFit.cover),
            ),
        child: Scaffold(
          //backgroundColor: Theme.of(context).colorScheme.background,
          //appBar: AppBar(),
          //appBar: NavBar().build(context: context),
          body: Padding(padding: EdgeInsets.only(top: statusBarHeight), child: DefaultTabController(
              length: widgets.length,
              child: TabBarView(
                physics: const NeverScrollableScrollPhysics(),
                controller: _tabController,
                children: widgets,
              ),
          ),),
          bottomNavigationBar: Container(
            padding: const EdgeInsets.all(8),
            margin: const EdgeInsets.all(12),
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(30)),
                color: Colors.transparent),
            child: TabBar(
              tabs: tabs,
              controller: _tabController,
              indicatorColor: Theme.of(context).colorScheme.onBackground,
              indicator: RectangularIndicator(
                bottomLeftRadius: 100,
                bottomRightRadius: 100,
                horizontalPadding: 10,
                topLeftRadius: 100,
                topRightRadius: 100,
                color: Theme.of(context).colorScheme.onBackground,
              ),
            ),
          ),
        ));
  }
}
