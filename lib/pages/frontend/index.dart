import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'home/home.dart';
import 'layout/main.dart';
import 'my_tours.dart';
import 'order/order.dart';
import 'profile.dart';
import 'setting.dart';
import '../../blocs/main/main_bloc.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainBloc, MainState>(
        bloc: BlocProvider.of<MainBloc>(context),
        builder: (context, state) {
          if (state is ReadyState) {
            List<AppPage> pages = List<AppPage>.empty(growable: true);
            pages.add(AppPage(
                  child: const HomeScreen(),
                  icon: const Icon(Icons.home, size: 20, color: Colors.white),
                  title: "Home"),);
            if(state.authenticate != null){
              pages.add(AppPage(
                child: const MyToursScreen(),
                icon: const Icon(Icons.tour, size: 20, color: Colors.white),
                title: "My Tours"));
              pages.add(AppPage(
                child: const OrderScreen(),
                icon: const Icon(Icons.grading, size: 20, color: Colors.white),
                title: "Order"));
              pages.add(AppPage(
                child: const ProfileScreen(),
                icon: const Icon(Icons.people, size: 20, color: Colors.white),
                title: "Profile"));
            }            
            pages.add(AppPage(
                  child: const SettingScreen(),
                  icon: const Icon(Icons.settings, size: 20, color: Colors.white),
                  title: "Settings"));
            return MainLayout(
              pages: pages,
            );
          }
          return const SizedBox();
        });
  }
}