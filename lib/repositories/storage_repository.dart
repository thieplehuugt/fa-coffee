import '../models/authenticate.dart';
import '../models/cart.dart';
import '../models/location.dart';
import '../models/trip.dart';
import 'local_repository.dart';
import 'remote_repository.dart';
import 'repository_interface.dart';

class StorageRepository implements Repository {
  late LocalRepository localStorage;
  late RemoteRepository remoteStorage;
  static StorageRepository _singleton = StorageRepository._internal();

  factory StorageRepository() {
    return _singleton;
  }

  StorageRepository._internal() {
    localStorage = LocalRepository();
    remoteStorage = RemoteRepository(url: 'http://192.168.100.25:8000/api');
  }

  static StorageRepository create() {
    _singleton = StorageRepository._internal();
    return _singleton;
  }

  @override
  Future<Authenticate>? login(
      {required String username, required String password}) {
    return remoteStorage.login(username: username, password: password);
  }

  @override
  Future<List<Location>>? getDestinations() {
    return remoteStorage.getDestinations();
  }

  @override
  Future<List<Trip>>? getTrips({String? name, int? destination, int? page}) {
    return remoteStorage.getTrips(
        name: name, destination: destination, page: page);
  }

  @override
  Future<bool> booking({required Cart cart}) async{
    return remoteStorage.booking(cart: cart);
  }
}
