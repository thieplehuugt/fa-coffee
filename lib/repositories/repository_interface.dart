import '../models/authenticate.dart';
import '../models/cart.dart';
import '../models/location.dart';
import '../models/trip.dart';

abstract class Repository {
  Future<Authenticate>? login({required String username, required String password});
  Future<List<Trip>>? getTrips({String? name, int? destination, int? page});
  Future<List<Location>>? getDestinations();
  Future<bool> booking({required Cart cart});
}