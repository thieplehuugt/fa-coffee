import 'dart:convert';

import 'package:http/http.dart' as http;
import '../models/authenticate.dart';
import '../models/cart.dart';
import '../models/location.dart';
import '../models/response_exeption.dart';
import '../models/trip.dart';
import 'repository_interface.dart';

class RemoteRepository implements Repository {
  final String url;
  RemoteRepository({required this.url});

  @override
  Future<Authenticate>? login(
      {required String username, required String password}) async {
    var response = await http.post(Uri.parse(url + '/auth/token'),
        headers: {"Content-Type": "application/json"},
        body: json.encode({'username': username, 'password': password}));
    final parsed = jsonDecode(response.body);
    if (parsed['code'] == 200) {
      return Authenticate.fromJson(parsed['data']);
    }
    return throw ResponseException(errors: List<String>.from(parsed['errors']));
  }

  @override
  Future<List<Location>>? getDestinations() async {
    var response = await http.get(
      Uri.parse(
        url + '/destinations/',
      ),
      headers: {"Content-Type": "application/json"},
    );
    final parsed = jsonDecode(response.body);
    if (parsed['code'] == 200) {
      return List<Location>.from(
          parsed['data'].map((x) => Location.fromJson(x)));
    }

    return throw ResponseException(errors: List<String>.from(parsed['errors']));
  }

  @override
  Future<List<Trip>>? getTrips(
      {String? name, int? destination, int? page}) async {
    var params = <String, dynamic>{};
    if (name != null) {
      params['name'] = name;
    }
    if (destination != null) {
      params['destination'] = destination;
    }
    if (destination != null) {
      params['page'] = page;
    }
    var response = await http.get(
      Uri.parse(
        url + '/posts/',
      ).replace(queryParameters: params),
      headers: {"Content-Type": "application/json"},
    );
    final parsed = jsonDecode(response.body);
    if (parsed['code'] == 200) {
      return List<Trip>.from(parsed['data'].map((x) => Trip.fromJson(x)));
    }

    return throw ResponseException(errors: List<String>.from(parsed['errors']));
  }

  @override
  Future<bool> booking({required Cart cart}) async {
    var response = await http.post(
        Uri.parse(
          url + '/booking/',
        ),
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          'price_package_id': cart.pricePackage.id,
          'adult_amount': cart.amountAdult,
          'child_amount': cart.amountChild,
        }));
    final parsed = jsonDecode(response.body);
    if (parsed['code'] == 200) {
      return true;
    }

    return throw ResponseException(errors: List<String>.from(parsed['errors']));
  }
}
