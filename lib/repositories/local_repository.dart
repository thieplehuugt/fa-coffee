import '../models/authenticate.dart';
import '../models/cart.dart';
import '../models/location.dart';
import '../models/trip.dart';
import 'repository_interface.dart';

class LocalRepository implements Repository {
  LocalRepository();

  @override
  Future<List<Trip>>? getTrips({String? name, int? destination, int? page}) {
    return null;
  }

  @override
  Future<Authenticate>? login(
      {required String username, required String password}) {
    return null;
  }

  @override
  Future<List<Location>>? getDestinations() {
    return null;
  }

  @override
  Future<bool> booking({required Cart cart}) async {
    return true;
  }
}
