import 'package:equatable/equatable.dart';

class Customer extends Equatable {
  final String name;
  final String email;
  final String phoneNumber;
  final String address;
  const Customer({
    required this.name,
    required this.email,
    required this.phoneNumber,
    required this.address,
  });

  @override
  List<Object> get props => [name, email, phoneNumber, address];
}
