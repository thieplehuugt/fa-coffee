import 'package:equatable/equatable.dart';

class Location extends Equatable {
  final int id;
  final String name;
  final String? thumbnail;
  final String? slug;
  const Location({
    required this.id,
    required this.name,
    this.thumbnail,
    this.slug,
  });

  factory Location.fromJson(Map<String, dynamic> json) {
    return Location(
      id: json['id'] ?? json['id'],
      name: json['name'] ?? json['name'],
      thumbnail: json['thumbnail'] ?? json['thumbnail'],
      slug: json['slug'] ?? json['slug'],
    );
  }

  @override
  List<Object> get props => [
        id,
        name,
        thumbnail != null,
      ];
}
