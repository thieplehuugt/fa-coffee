import 'dart:convert';

import 'package:flutter/cupertino.dart';

import 'section.dart';
import 'tab_index.dart';

class TabView {
  final TabIndex index;
  final String name;
  final List<Section> sections;
  final IconData? icon;
  const TabView({
    required this.index,
    required this.name,
    required this.sections,
    this.icon,
  });

  factory TabView.fromJson(Map<String, dynamic> json) {
    return TabView(
      index: getTabIndex(json['name']),
      name: json['name'],
      sections: json['content'] != null
          ? List<Section>.from(
              jsonDecode(json['content']).map((x) => Section.fromJson(x)))
          : [],
    );
  }

  static TabIndex getTabIndex(name) {
    switch (name) {
      case "Overview":
        return TabIndex.overview;
      case "Itinerary":
        return TabIndex.itinerary;
      case "Includes/Excludes":
        return TabIndex.includesExclude;
      case "Map":
        return TabIndex.map;
      case "Child & Refund Policy":
        return TabIndex.childRefund;
      default:
        return TabIndex.overview;
    }
  }
}
