import 'package:equatable/equatable.dart';

class Duration extends Equatable {
  final int id;
  final String name;
  const Duration({
    required this.id,
    required this.name,
  });
  
  factory Duration.fromJson(Map<String, dynamic> json) {
    return Duration(
      id: json['id'] ?? json['id'],
      name: json['name'] ?? json['name'],
    );
  }
  
  @override
  List<Object> get props => [id, name, ];
}