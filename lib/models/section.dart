import 'package:equatable/equatable.dart';

class Section extends Equatable {
  final String? title;
  final String content;
  const Section({
    this.title,
    required this.content,
  });

  factory Section.fromJson(Map<String, dynamic> json) {
    return Section(
      title: json['name'] ?? json['name'],
      content: json['content'] ?? json['content'],
    );
  }

  @override
  List<Object> get props => [
        title!=null,
        content,
      ];
}
