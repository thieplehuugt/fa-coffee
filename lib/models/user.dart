class User {
  final int id;
  final String username;
  final String email;
  final int mobile;
  final String? address1;
  final String? address2;
  final DateTime? birth;
  final String? avatar;
  User({
    required this.id,
    required this.username,
    required this.email,
    required this.mobile,
    required this.address1,
    required this.address2,
    required this.birth,
    required this.avatar,
  });

  User.fromJson(Map<String, dynamic> data)
      : id = data['id'],
        username = data['username'],
        email = data['email'],
        mobile = data['mobile'],
        address1 = data['address1'],
        address2 = data['address2'],
        birth = data['birth'] != null ? DateTime.parse(data['birth']) : null,
        avatar = data['avatar'];

  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'email': email,
      'mobile': mobile,
      'address1': address1,
      'address2': address2,
      'birth': birth,
      'avatar': avatar,
    };
  }
}
