import 'media.dart';
import 'tab_view.dart';

class PricePackage {
  final int id;
  final String name;
  final double? childPrice;
  final double adultPrice;
  final int minPerson;
  final int maxPerson;
  PricePackage({
    required this.id,
    required this.name,
    required this.adultPrice,
    this.childPrice,
    required this.minPerson,
    required this.maxPerson,
  });
  
  PricePackage.fromJson(Map<String, dynamic> data)
      : id = data['id'],
        name = data['name'],
        adultPrice = data['adult_price'],
        childPrice = data['child_price'],
        minPerson = data['min_person'],
        maxPerson = data['max_person'];

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'adule_price': adultPrice,
      'child_price': childPrice,
      'minPerson': minPerson,
      'maxPerson': maxPerson,
    };
  }
}

class Price {
  final double minPrice;
  final double maxPrice;
  final List<PricePackage>? packages;
  const Price({
    this.minPrice = 0,
    this.maxPrice = 0,
    this.packages,
  });

  Price copyWith({double minPrice = 0, double maxPrice = 0, List<PricePackage>? packages}) => Price(
        minPrice: minPrice,
        maxPrice: maxPrice,
        packages: packages ?? this.packages,
      );
  
  
  Price.fromJson(Map<String, dynamic> data)
      : minPrice = data['min_price'],
        maxPrice = data['max_price'],
        packages = List<PricePackage>.from(data['packages'].map((x) => PricePackage.fromJson(x)));

  Map<String, dynamic> toJson() {
    return {
      'min_price': minPrice,
      'max_price': maxPrice,
      'packages':  packages ?? packages!.map((i) => i.toJson()).toList(),
    };
  }
}

class Trip {
  final String title;
  final List<TabView> tabs;
  final String? thumbnail;
  final List<Media>? images;
  final Price price;
  Trip({
    required this.title,
    required this.tabs,
    required this.price,
    required this.thumbnail,
    this.images,
  });

  Trip.fromJson(Map<String, dynamic> data)
      : title = data['title'],
        tabs = List<TabView>.from(data['metadatas'].map((x) => TabView.fromJson(x))),
        thumbnail = data['thumbnail'],
        price = Price.fromJson(data['price']),
        images = List<Media>.from(data['medias'].map((x) => Media.fromJson(x)));

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'metadatas': tabs,
      'thumbnail': thumbnail,
      'price': price.toJson(),
      'images': images,
    };
  }
}
