import 'user.dart';

class Authenticate {
  final String accessToken;
  final String tokenType;
  final User user;
  Authenticate({
    required this.accessToken,
    required this.tokenType,
    required this.user,
  });

  Authenticate.fromJson(Map<String, dynamic> data): 
  user = User.fromJson(data['user']), 
  accessToken = data['access_token'], 
  tokenType = data['token_type'];

  Map<String, dynamic> toJson() {
    return {
      'user': user,
    };
  }
}
