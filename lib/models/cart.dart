import 'package:equatable/equatable.dart';

import 'customer.dart';
import 'trip.dart';

class Cart extends Equatable {
  final int amountAdult;
  final int amountChild;
  final PricePackage pricePackage;
  final Customer? customer;
  final double total;
  const Cart({
    required this.amountAdult,
    required this.amountChild,
    required this.pricePackage,
    this.customer,
    required this.total,
  });

  @override
  List<Object> get props => [amountAdult, amountChild, pricePackage, total];
}
