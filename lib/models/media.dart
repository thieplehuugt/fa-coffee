class Media {
  final int id;
  final String type;
  final String url;
  Media({
    required this.id,
    required this.type,
    required this.url,
  });

  Media.fromJson(Map<String, dynamic> data)
      : id = data['id'],
      type = data['type'],
      url = data['url'];

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'type': type,
      'url': url,
    };
  }
}
