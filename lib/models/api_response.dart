class ApiResponse {
  final int code;
  final dynamic data;
  final List<String> errors;
  const ApiResponse({
    required this.code,
    required this.data,
    this.errors = const [],
  });
  
  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return ApiResponse(
      code: json['code'] ?? json['code'],
      data: json['data'] ?? json['data'],
      errors: json['errors'] ?? json['errors'],
    );
  }  
}