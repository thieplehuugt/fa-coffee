
class ResponseException implements Exception {
  final List<String> errors;
  const ResponseException({
    required this.errors,
  });  
}