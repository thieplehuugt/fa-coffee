import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class PhotoSlider extends StatelessWidget {
  final List<String> photos;
  const PhotoSlider({required this.photos, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return CarouselSlider(
      options: CarouselOptions(
        height: height,
        viewportFraction: 1.0,
        enlargeCenterPage: false,
        autoPlay: true,
      ),
      items: photos.map((photo) {
        return Builder(
          builder: (BuildContext context) {
            return CachedNetworkImage(
              imageUrl: photo,
              fit: BoxFit.contain,
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            );
          },
        );
      }).toList(),
    );
  }
}
