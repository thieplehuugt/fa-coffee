import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/main/main_bloc.dart';
import '../pages/frontend/trip/trip.dart';
import 'trip_card.dart';

class TripsCard extends StatelessWidget {
  final VoidCallback? onTap;
  const TripsCard({this.onTap, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainBloc, MainState>(
        bloc: BlocProvider.of<MainBloc>(context),
        builder: (context, state) {
          if (state is ReadyState) {
            List<Widget> children = List<Widget>.empty(growable: true);
            children.add(Text("Best trips for you", style: Theme.of(context).textTheme.headline5!.copyWith(color: Theme.of(context).colorScheme.onPrimary),));
            for (var trip in state.trips) {
              children.add(TripCard(
                trip: trip,
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => TripPage(
                              trip: trip,
                            )),
                  );
                },
              ));
            }
            return Column(crossAxisAlignment: CrossAxisAlignment.start,
              children: children);
          }
          return const SizedBox(
            height: 0,
          );
        });
  }
}
