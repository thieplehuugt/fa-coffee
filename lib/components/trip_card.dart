import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../constants.dart';
import '../models/trip.dart';

class TripCard extends StatelessWidget {
  final Trip trip;
  final VoidCallback? onTap;
  const TripCard({required this.trip, this.onTap, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double cardWidth = width - horizontalPadding * 2; // padding
    double cardHeight = cardWidth * 3 / 4;
    return InkWell(
      onTap: () => onTap!(),
      child: Card(
        margin: const EdgeInsets.only(top: 12, bottom: 12),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Stack(
          children: [
            Container(
              width: cardWidth,
              height: cardHeight,
              decoration:
                BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.darken),
                    image: CachedNetworkImageProvider(
                      trip.thumbnail!,
                    ),
                  ),
                ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                padding: const EdgeInsets.all(12),
                width: cardWidth,
                child: Align(
                  alignment: AlignmentDirectional.bottomEnd, // <-- SEE HERE
                  child:Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            trip.title,
                            style: Theme.of(context)
                                .textTheme
                                .headline6!.copyWith(color: Color.fromARGB(255, 235, 251, 255), fontWeight: FontWeight.w600),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Align(
                            alignment: AlignmentDirectional.topEnd,
                            child: Text("From: " + trip.price.minPrice.toString() + "\$",
                              style: Theme.of(context).textTheme.subtitle2!.copyWith(color: Colors.white)),
                          )
                        ]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
