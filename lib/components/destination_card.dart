import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../models/location.dart';

class DestinationCard extends StatelessWidget {
  final Location location;
  final VoidCallback? onTap;
  final double? width;
  final double? height;
  const DestinationCard({required this.location, this.width, this.height, this.onTap, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double cardWidth = screenWidth - 24; // padding
    double cardHeight = cardWidth * 3 / 4;
    return InkWell(
      onTap: () => onTap!(),
      child: Card(
        margin: const EdgeInsets.only(top: 12, bottom: 12),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Stack(
          children: [
            SizedBox(
              width: width ?? cardWidth,
              height: height ?? cardHeight,
              child: CachedNetworkImage(
                imageUrl:
                    location.thumbnail ?? "https://via.placeholder.com/300x400",
                fit: BoxFit.cover,
                placeholder: (context, url) =>
                    const Center(child: SizedBox(width: 64, height: 64, child:CircularProgressIndicator()),),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                padding: const EdgeInsets.all(12),
                color: Colors.black.withOpacity(0.3),
                width: cardWidth,
                child: Text(
                  location.name,
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(color: Colors.white, fontWeight: FontWeight.w600),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
