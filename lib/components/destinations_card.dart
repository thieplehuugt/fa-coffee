import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/main/main_bloc.dart';
import '../constants.dart';
import '../pages/frontend/trips/trips.dart';
import 'destination_card.dart';

class DestinationsCard extends StatelessWidget {
  final VoidCallback? onTap;
  const DestinationsCard({this.onTap, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double padding = verticalPadding; // padding
    double width = MediaQuery.of(context).size.width;
    double cardHeight = (width - padding) * 2 / 3;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Destinations", style: Theme.of(context).textTheme.headline5!.copyWith(color: Theme.of(context).colorScheme.onPrimary),),
        SizedBox(
          height: cardHeight,
          child: BlocBuilder<MainBloc, MainState>(
              bloc: BlocProvider.of<MainBloc>(context),
              builder: (context, state) {
                if (state is ReadyState) {
                  var destinations = state.locations;
                  return CarouselSlider.builder(        
                    options: CarouselOptions(
                      height: cardHeight,
                      viewportFraction: 1,
                      aspectRatio: 2.0,
                      autoPlay: true,
                    ),
                    itemCount: (destinations.length / 2).round(),
                    itemBuilder: (context, index, realIdx) {
                      final int first = index * 2;
                      final int second = first + 1;
                      return Builder(
                        builder: (BuildContext context) {
                          return Row(
                            children: [first, second].map((idx) {
                              return Expanded(
                                flex: 1,
                                child: Container(
                                  margin: EdgeInsets.only(left: (idx == first) ? 0 : padding/2, right: (idx == second) ? 0 : padding/2),
                                  child: DestinationCard(
                                    location: destinations[idx],
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => const TripsPage()),
                                      );
                                    },
                                  ),
                                ),
                              );
                            }).toList(),
                          );
                        },
                      );
                    },
                  );                  
                }
                return const SizedBox(
                  height: 0,
                );
              }),
        )
      ],
    );
  }
}
