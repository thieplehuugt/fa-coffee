import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';

part 'network_event.dart';
part 'network_state.dart';

class NetworkBloc extends Bloc<ConnectivityEvent, NetworkState> {
  StreamSubscription? subscription;
  NetworkBloc() : super(NetworkInitial()) {
    on<OnConnectedEvent>((event, emit) => emit(NetworkConnectedState()));
    on<OnNotConnectedEvent>((event, emit) => emit(NetworkDisconnectedState()));
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) async {
      if (result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi) {
        try {
          final result = await InternetAddress.lookup('www.google.com'); 
          var res = result.isNotEmpty && result[0].rawAddress.isNotEmpty;
          if(res){
            add(OnConnectedEvent());
          }else{
            add(OnNotConnectedEvent());
          }
        } on SocketException catch (_) {
          add(OnNotConnectedEvent());
        }
        add(OnConnectedEvent());
      } else {
        add(OnNotConnectedEvent());
      }
    });
  }
}
