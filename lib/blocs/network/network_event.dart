part of 'network_bloc.dart';

abstract class ConnectivityEvent extends Equatable {
  const ConnectivityEvent();

  @override
  List<Object> get props => [];
}

class OnConnectedEvent extends ConnectivityEvent{}

class OnNotConnectedEvent extends ConnectivityEvent{}