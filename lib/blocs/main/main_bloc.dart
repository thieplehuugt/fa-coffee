import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '../../models/authenticate.dart';
import '../../models/location.dart';
import '../../models/media.dart';
import '../../models/response_exeption.dart';
import '../../models/section.dart';
import '../../models/tab_index.dart';
import '../../models/tab_view.dart';
import '../../models/trip.dart';
import '../../repositories/storage_repository.dart';

import '../../models/network_status.dart';
import '../network/network_bloc.dart';

part 'main_event.dart';
part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final NetworkBloc connectivityBloc;
  final StorageRepository repository;
  MainBloc({required this.connectivityBloc, required this.repository})
      : super(MainInitial()) {
    connectivityBloc.stream.listen((connectivityState) {
      var networkStatus = NetworkStatus.disconnected;
      if (connectivityState is NetworkConnectedState) {
        networkStatus = NetworkStatus.connected;
      } else if (state is NetworkDisconnectedState) {
        networkStatus = NetworkStatus.disconnected;
      }
      if (state is MainInitial) {
        add(LoadDataEvent(status: networkStatus));
      } else {
        add(UpdateNetworkStatus(status: networkStatus));
      }
    });
    on<UpdateNetworkStatus>(
        (event, emit) => updateNetworkStatus(emit, event.status));
    on<LoginEvent>(
        (event, emit) => login(emit, event.username, event.password));
    on<LoadDataEvent>((event, emit) => loadData(emit, event.status));
  }

  updateNetworkStatus(emit, NetworkStatus status) {
    emit(
      ReadyState(
          status: status,
          trips: (state as ReadyState).trips,
          locations: (state as ReadyState).locations,
          myTrips: (state as ReadyState).myTrips),
    );
  }

  login(emit, String username, String password) async {
    if (username.isEmpty || password.isEmpty) {
      emit(
        const LoginFailureState(errors: ["Email and Password cannot empty"]),
      );
    }
    try {
      var authenticate =
          await repository.login(username: username, password: password);
      if (authenticate != null) {
        emit(
          ReadyState(
              authenticate: authenticate,
              status: (state as ReadyState).status,
              trips: (state as ReadyState).trips,
              locations: (state as ReadyState).locations,
              myTrips: (state as ReadyState).myTrips),
        );
      }
    } on ResponseException catch (e) {
      emit(
        LoginFailureState(errors: e.errors),
      );
    }
  }

  loadData(emit, NetworkStatus status) async {
    //var trips = await  repository.getTrips();
    //var locations = await repository.getDestinations();
    var locations = [
      Location(
        id:1,
        name:"Hanoi",
        thumbnail:"https://media.vietravel.com/images/news/thumb-ha-noi-mua-thu_3.jpg",
      ),
      Location(
        id:2,
        name:"Ho Chi Minh",
        thumbnail:"https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/06/20/16/ho-chi-minh-city.jpg?width=1200",
      ),
      Location(
        id:3,
        name:"Can Tho",
        thumbnail:"https://media.istockphoto.com/photos/farmers-purchase-crowded-in-floating-market-morning-picture-id811264106?k=20&m=811264106&s=612x612&w=0&h=49Cqnsq2wm31vXtL_usa8XOnLcMJgwRwLNDOQSzebb8=",
      ),
      Location(
        id:4,
        name:"Da Nang",
        thumbnail:"https://media.istockphoto.com/photos/danang-vietnam-view-of-dragon-bridge-picture-id1156033272?b=1&k=20&m=1156033272&s=170667a&w=0&h=GVw-30cIQ1AMmgOxQyoCn-XwJLqTxwuUTtbEVFa23P4=",
      ),
    ];
    var images = [
      'http://gaixinhvietnam.2019.vn/wp-content/uploads/2018/10/anh-girl-xinh-9x-de-thuong.jpg',
      'http://gaixinhvietnam.2019.vn/wp-content/uploads/2018/10/junvu95_12530674_925191860928396_810815259_n.jpg',
      'https://1.bp.blogspot.com/-6cx2g48sid8/XfXnMYbeZ8I/AAAAAAAAUSM/55zY2IBYz3Al2QuxLK8Q2RlcQuVt4caMwCLcBGAsYHQ/s1600/Hinh-Anh-Hot-Girl-Facebook-Wap102-Com%2B%25281%2529.jpg',
      'https://1.bp.blogspot.com/-gGEY7S-VL7k/XfXnRnmttiI/AAAAAAAAUS8/joTLkA-Jm2YFYRMweXcJe9AtKf0WGiY4ACLcBGAsYHQ/s1600/Hinh-Anh-Hot-Girl-Facebook-Wap102-Com%2B%252811%2529.jpg',
      'https://luv.vn/wp-content/uploads/2021/08/hinh-anh-gai-xinh-71.jpg',
      'https://luv.vn/wp-content/uploads/2021/08/hinh-anh-gai-xinh-6.jpg',
      'https://thuthuatnhanh.com/wp-content/uploads/2019/08/girl-cap-3-deo-kinh-578x580.jpg',
      'https://thuthuatnhanh.com/wp-content/uploads/2019/08/hinh-girl-xinh-deo-kinh-tri-thuc-387x580.jpeg'
    ];
    var trips = List<Trip>.empty(growable: true);
    final _random = Random();
    for (var i = 40; i >= 1; i--) {
      var image = images[_random.nextInt(images.length)];
      trips.add(
        Trip(
          title:
              "Ho Chi Minh City – Cu Chi-Ben Duoc Tunnels full day (original and less touristy tunnels section)",
          tabs: [
            const TabView(
                index: TabIndex.overview,
                name: "Oveview",
                icon: Icons.topic_sharp,
                sections: [
                  Section(
                      content:
                          "For a long time, Vietnam has been famous to international friends as a country that has experienced many years of war with exciting historical victories. "
                          "One of the tourist sites that have been attracting visitors to broaden their knowledge of historical stories is the Cu Chi tunnels, "
                          "which were once used as a military base, a hospital, and more during wartime. From there, visitors will be amazed at what the Vietnamese soldiers overcame during that challenging time."
                          "HIGHLIGHTS "
                          "– Get to visit a “less popular tourist” tunnel site – Bến Dược Tunnel."
                          "– Enjoy two attractive tourist destinations in just one day – Time-saving for any traveler."
                          "– Served by hospitable and professional staff, coupled with clean transportation."
                          "– The number of guests on the tour is small – Easy for tourists to enjoy a fascinating trip.")
                ]),
            const TabView(
                index: TabIndex.itinerary,
                name: "Itinerary",
                icon: Icons.timeline,
                sections: [
                  Section(
                      title:
                          "Saigon - Cu Chi Tunnels (it takes 1 hour 30 minutes drive)",
                      content:
                          "7:30 AM: The tour guide will pick you up at the hotel in a new car (fully equipped with cold towels, water, and additional utilities), which will take you to the main point of the tour – Củ Chi Tunnels."),
                  Section(
                      title: "Arriving Cu Chi Tunnels (visit about 2 hours)",
                      content:
                          "When you get there, you will be able to see firsthand the remnants of wartime, the secret bunkers used as the military shelters for Vietnamese soldiers, "
                          "watch the documentary film, and understand more deeply how Vietnamese people could fight bravely to gain our own independence."
                          "You can experience crouching, even crawling in those tunnels to imagine how terrible people’s lives were in the past."
                          "Shooting with different types of guns, especially the AK47 shall be available at your own expense if you want some ‘shooting time’ (Bullets cost excludes)."
                          "Before saying goodbye to this place, you will get to try tapioca and tea, which were the daily dishes of the soldiers in the past."),
                  Section(
                      title:
                          "Go back to Saigon (it takes 1 hour 30 minutes drive)",
                      content:
                          "Our guide and drive shall take you back to your hotel. Tour ends"
                          "Thank you so much for joining the tour with us and see you again."
                          "Have a nice trip in Vietnam!"),
                ]),
            const TabView(
                index: TabIndex.includesExclude,
                icon: Icons.add_task,
                name: "Includes \r\nExcludes",
                sections: [
                  Section(
                      title: "The Tour Included",
                      content:
                          "Local English Local English speaking guide, A/C transportation, entrance tickets, tropical fruits, drinking water, tissue."),
                  Section(
                      title: "The Tour Excluded",
                      content:
                          "Items of a personal nature, additional food, and drinks, tips/gratuities for the local guide."),
                ]),
            const TabView(
                index: TabIndex.map,
                icon: Icons.map,
                name: "Map",
                sections: [
                  Section(
                      content:
                          "Oveview Speaking of Ho Chi Minh City (Saigon), you will probably think of busy shopping areas, bustling street corners, passing cars, shimmering lights of skyscrapers. Besides, there are still ancient corners that have remained strong despite the time."
                          "You will be picked at your hotel to discover the cultural marks of this city:"
                          "War Remnant Museum"
                          "Notre Dame Cathedral"
                          "Old Central Post Office"
                          "Jade Pagoda"
                          "Opera House"
                          "City hall – Walking Street"
                          "Lunch will be served in a local restaurant Option 1 ends once we lead you back to the hotel."
                          "OPTION 2: Cu Chi – Ben Duoc Tunnels Afternoon Extension (original and less touristy tunnels section)"
                          "After lunch, you will be transferred to Cu Chi – Ben Duoc Tunnels with a one and half hour drive. This is a rare chance to escape the bustle of the city and explore the rural scenery along the way.")
                ]),
            const TabView(
                index: TabIndex.childRefund,
                icon: Icons.checklist_rounded,
                name: "Child \r\nRefund Policy",
                sections: [
                  Section(
                      title: "Children’s Ticket Policy",
                      content: "– Children from 11 years old up: Full ticket"
                          "– Children from 5 up to 11 years old: 75% of the ticket"
                          "– Infants under 5 years old: F.O.C (Children’s expense on the Tour shall be paid by their parents)"),
                  Section(
                      title: "Refund Policy",
                      content:
                          "When you book a reservation for a Tour or Service through our Website, full payment by credit, debit card, bank transfer or Paypal is required to make a reservation. We, in some cases, accept ‘Payment upon Departure’ for some bookings which are to close to the departure time and these bookings are only received and agreed by our top manager."
                          "No refunds are available once a tour or service has commenced, or in respect of any package, accommodation, meals or any other services utilized."
                          "If you wish to cancel a reservation made via the Website, this cancellation policy will apply to, and govern the terms of, your cancellation and any refunds. When canceling a booking, you will need to contact us and raise your demand of the total refund amount you will receive."
                          "The standard cancellation policy for Hana Tourist is:"
                          "a 100% refund for cancellations made at least 24 hours in advance of the start date of the experience."
                          "Cancellations made within 24 hours of the start date of the experience will receive no refund. However, we will have full rights to decide if the Refund Claim is reasonable for the cancellations made during this timeline."
                          "The value of the transaction may be subject to taxes, duties, foreign transaction, currency exchange or other fees."),
                ]),
          ],
          price: Price(minPrice: 50, maxPrice: 80, packages: [
            PricePackage(
                id: 1,
                name: "1-2 Pax",
                childPrice: 109,
                adultPrice: 120,
                minPerson: 1,
                maxPerson: 2),
            PricePackage(
                id: 1,
                name: "3-4 Pax",
                childPrice: 95,
                adultPrice: 110,
                minPerson: 3,
                maxPerson: 4),
            PricePackage(
                id: 1,
                name: "5-6 Pax",
                childPrice: 85,
                adultPrice: 100,
                minPerson: 5,
                maxPerson: 6),
            PricePackage(
                id: 1,
                name: "7-8 Pax",
                childPrice: 75,
                adultPrice: 90,
                minPerson: 7,
                maxPerson: 8),
            PricePackage(
                id: 1,
                name: "9-10 Pax",
                childPrice: 65,
                adultPrice: 80,
                minPerson: 9,
                maxPerson: 10),
            PricePackage(
                id: 1,
                name: "11-12 Pax",
                childPrice: 55,
                adultPrice: 70,
                minPerson: 11,
                maxPerson: 12),
            PricePackage(
                id: 1,
                name: "13-14 Pax",
                childPrice: 45,
                adultPrice: 60,
                minPerson: 12,
                maxPerson: 14),
            PricePackage(
                id: 1,
                name: "15-16 Pax",
                childPrice: 35,
                adultPrice: 50,
                minPerson: 15,
                maxPerson: 16),
            PricePackage(
                id: 1,
                name: "17-18 Pax",
                childPrice: 25,
                adultPrice: 45,
                minPerson: 17,
                maxPerson: 18),
            PricePackage(
                id: 1,
                name: "19-20 Pax",
                childPrice: 15,
                adultPrice: 35,
                minPerson: 19,
                maxPerson: 20)
          ]),
          thumbnail: image,
          images: [
            Media(
                id: 1,
                type: 'image',
                url: images[_random.nextInt(images.length)]),
            Media(
                id: 2,
                type: 'image',
                url: images[_random.nextInt(images.length)]),
            Media(
                id: 3,
                type: 'image',
                url: images[_random.nextInt(images.length)]),
            Media(
                id: 4,
                type: 'image',
                url: images[_random.nextInt(images.length)]),
            Media(
                id: 5,
                type: 'image',
                url: images[_random.nextInt(images.length)]),
            Media(
                id: 6,
                type: 'image',
                url: images[_random.nextInt(images.length)]),
            Media(
                id: 7,
                type: 'image',
                url: images[_random.nextInt(images.length)]),
            Media(
                id: 8,
                type: 'image',
                url: images[_random.nextInt(images.length)]),
            Media(
                id: 9,
                type: 'image',
                url: images[_random.nextInt(images.length)]),
          ],
        ),
      );
    }

    var myTrips = List<Trip>.empty(growable: true);
    emit(ReadyState(
      status: status,
      trips: trips,
      locations: locations!,
      myTrips: myTrips,
    ));
  }
}
