part of 'main_bloc.dart';

abstract class MainEvent extends Equatable {
  const MainEvent();

  @override
  List<Object> get props => [];
}

class UpdateNetworkStatus extends MainEvent {
  final NetworkStatus status;
  const UpdateNetworkStatus({required this.status});
  @override
  List<Object> get props => [status];
}

class LoginEvent extends MainEvent {
  final String username;
  final String password;
  const LoginEvent({required this.username, required this.password});
  
  @override
  List<Object> get props => [username, password];
}

class LoadDataEvent extends MainEvent {
  final NetworkStatus status;
  const LoadDataEvent({required this.status});

  @override
  List<Object> get props => [status];
}

class LoadDataFailureEvent extends MainEvent {}

class AddMyTripEvent extends MainEvent {
  final Trip trip;
  const AddMyTripEvent({required this.trip,});
  
  @override
  List<Object> get props => [trip];
}
