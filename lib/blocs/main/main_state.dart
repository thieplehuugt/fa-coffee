part of 'main_bloc.dart';

abstract class MainState extends Equatable {
  const MainState();

  @override
  List<Object?> get props => [];
}

class MainInitial extends MainState {}

class NetworkStatusState extends MainState {}

class ReadyState extends MainState {
  final Authenticate? authenticate;
  final NetworkStatus status;
  final List<Trip> trips;
  final List<Location> locations;
  final List<Trip> myTrips;
  const ReadyState(
      {this.authenticate,
      required this.status,
      required this.trips,
      required this.locations,
      required this.myTrips});
  @override
  List<Object?> get props => [authenticate, status, trips, locations, myTrips];
}

class LoginFailureState extends MainState {
  final List<String> errors;
  const LoginFailureState({required this.errors});
}

class LoadingDataFailureState extends MainState {}

class LoadDataFailureState extends MainState {}
